# member.data.coop

To start developing:

Get poetry

    $ python3 -m pip install --user pipx
    $ pipx install poetry

Run poetry to setup environment

    $ poetry install

Run this make target, which installs all the requirements and sets up a development database.

    $ make dev-setup

To run the Django development server:

    $ poetry run python manage.py runserver

Before you push your stuff, run tests:

    $ make test
