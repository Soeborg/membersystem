"""URLs for the membersystem"""
from django.contrib import admin
from django.urls import include
from django.urls import path

from . import views

urlpatterns = [
    path("", views.index),
    path("users/", include("users.urls")),
    path("admin/", admin.site.urls),
]
