import warnings

from .base import *  # noqa

try:
    from .local import *  # noqa
except ImportError:
    warnings.warn(
        "No settings.local, using a default SECRET_KEY 'hest'. You should "
        "write a custom local.py with this setting."
    )
    SECRET_KEY = "hest"
    DEBUG = True
    pass
