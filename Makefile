# These are just some make targets, expressing how things
# are supposed to be run, but feel free to change them!

dev-setup:
	poetry run pre-commit install
	poetry run python manage.py migrate
	poetry run python manage.py createsuperuser

lint:
	poetry run pre-commit run --all

test:
	poetry run pytest
