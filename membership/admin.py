from django.contrib import admin

from . import models


@admin.register(models.Membership)
class MembershipAdmin(admin.ModelAdmin):
    pass
