from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext as _
from djmoney.models.fields import MoneyField


class CreatedModifiedAbstract(models.Model):

    modified = models.DateTimeField(auto_now=True, verbose_name=_("modified"))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("created"))

    class Meta:
        abstract = True


class Organization(CreatedModifiedAbstract):
    """
    This holds the data of the organization that someone is a member of. It is
    possible that we'll create more advanced features here.
    """

    name = models.CharField(verbose_name=_("name"), max_length=64)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("organization")
        verbose_name_plural = _("organizations")


class Membership(CreatedModifiedAbstract):
    """
    A user remains a member of an organization even though the subscription is
    unpaid or renewed. This just changes the status/permissions etc. of the
    membership, thus we need to track subscription creation, expiry, renewals
    etc. and ensure that the membership is modified accordingly.

    This expresses some
    """

    organization = models.ForeignKey(Organization, on_delete=models.PROTECT)
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)

    can_vote = models.BooleanField(
        default=False,
        verbose_name=_("can vote"),
        help_text=_(
            "Indicates that the user has a democratic membership of the "
            "organization."
        ),
    )

    def __str__(self):

        return _("{} is a member of {}").format(
            self.user.get_full_name(), self.organization.name
        )

    class Meta:
        verbose_name = _("membership")
        verbose_name_plural = _("memberships")


class SubscriptionType(CreatedModifiedAbstract):
    """
    Properties of subscriptions are stored here. Should of course not be edited
    after subscriptions are created.
    """

    organization = models.ForeignKey(Organization, on_delete=models.PROTECT)

    name = models.CharField(verbose_name=_("name"), max_length=64)

    fee = MoneyField(max_digits=16, decimal_places=2)

    fee_vat = MoneyField(max_digits=16, decimal_places=2, default=0)

    duration = models.PositiveSmallIntegerField(
        default=1, choices=[(1, _("annual"))], verbose_name=_("duration")
    )

    class Meta:
        verbose_name = _("subscription type")
        verbose_name_plural = _("subscription types")


class Subscription(CreatedModifiedAbstract):
    """
    To not confuse other types of subscriptions, one can be a *subscribed*
    member of an organization, meaning that they are paying etc.

    A subscription does not track payment, this is done in the accounting app.
    """

    subscription_type = models.ForeignKey(
        SubscriptionType,
        related_name="memberships",
        verbose_name=_("subscription type"),
        on_delete=models.PROTECT,
    )
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)

    active = models.BooleanField(
        default=False,
        verbose_name=_("active"),
        help_text=_("Automatically set by payment system."),
    )

    starts = models.DateField()
    ends = models.DateField()

    renewed_subscription = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        verbose_name=_("renewed subscription"),
        on_delete=models.PROTECT,
    )

    class Meta:
        verbose_name = _("subscription")
        verbose_name_plural = _("subscriptions")
